new Vue({
  el: "#main",
  data: {
    loginError: "",
    usernameError: "",
    emailError: "",
    passwordError: "",
    confirmPasswordError: "",
    nameError: "",
    surnameError: "",

    signupUsername: "",
    email: "",
    signupPassword: "",
    confirmPassword: "",
    name: "",
    surname: "",
    gender: "male",

    loginUsername: "",
    loginPassword: ""
  },
  methods: {
    signup: function (e) {
      let allGood = true;
      if (this.signupUsername.length < 5 || this.signupUsername.length > 15) {
        this.usernameError = "Username must be 5-15 characters long!";
        allGood = false;
        e.preventDefault();
      } else if (!/^[A-Za-z0-9_]+$/.test(this.signupUsername)) {
        this.usernameError =
          "Username must contain only letters, numbers and underscores!";
        allGood = false;
        e.preventDefault();
      }
      if (!/\S+@\S+\.\S+/.test(this.email)) {
        this.emailError = "Invalid e-mail format!";
        allGood = false;
        e.preventDefault();
      }
      if (this.signupPassword.length < 5 || this.signupPassword.length > 15) {
        this.passwordError = "Password length must be 5-15!";
        allGood = false;
        e.preventDefault();
      }
      if (this.signupPassword !== this.confirmPassword) {
        this.confirmPasswordError = "Passwords do not match!";
        allGood = false;
        e.preventDefault();
      }
      if (!/^[A-Za-z]+$/.test(this.name)) {
        this.nameError = "Invalid name!";
        allGood = false;
        e.preventDefault();
      }
      if (!/^[A-Za-z]+$/.test(this.surname)) {
        this.surnameError = "Invalid surname!";
        allGood = false;
        e.preventDefault();
      }
      if (allGood) {
        var su = {
          signupUsername: this.signupUsername,
          email: this.email,
          signupPassword: this.signupPassword,
          confirmPassword: this.confirmPassword,
          name: this.name,
          surname: this.surname,
          gender: this.gender
        };
        axios
          .post("/signup", su)
          .then((response) => {
            let res = response.data;
            if (res.status === "success") {
              usernameError = "";
              emailError = "";
              passwordError = "";
              confirmPasswordError = "";
              nameError = "";
              surnameError = "";
              alert("Registration successful!");
              document.location.reload();
            }
          })
          .catch((error) => {
            if (error.response) {
              let res = error.response.data;
              if (res.info === "username taken") {
                this.usernameError = "This username is already taken!";
                e.preventDefault();
              } else if (res.info === "invalid sign up data") {
                this.usernameError = "Invalid data. Please try again!";
                e.preventDefault();
              }
            }
          });
      }
    },
    deleteError: function (error) {
      if (
        error === "Username must be 5-15 characters long!" ||
        error ===
          "Username must contain only letters, numbers and underscores!" ||
        error === "This username is already taken!"
      )
        this.usernameError = "";
      else if (error === "Invalid e-mail format!") this.emailError = "";
      else if (error === "Passwords do not match!")
        this.confirmPasswordError = "";
      else if (error === "Password length must be 5-15!")
        this.passwordError = "";
      else if (error === "Invalid name!") this.nameError = "";
      else if (error === "Invalid surname!") this.surnameError = "";
      else if (error === "Invalid username or password!") this.loginError = "";
    },
    login: function (e) {
      var lu = { username: this.loginUsername, password: this.loginPassword };
      axios
        .post("/login", lu)
        .then((response) => {
          if (response.data.status === "success") {
            this.loginError = "";
            window.location.href = "../app/home.html";
          }
        })
        .catch((error) => {
          if (error.response) {
            let res = error.response.data;
            if (res.info === "invalid username or password") {
              this.loginError = "Invalid username or password!";
              e.preventDefault();
            }
          }
        });
    }
  }
});
