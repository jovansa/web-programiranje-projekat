new Vue({
  el: "#main",
  data: {
    tooltipString:
      "Date range in which the user was born can be specified with the date range input. <u>Important</u>: Leave the default range (today's date - today's date) in order to ignore date search! <br/> <i><u>Note</u>: Administrators can search users by e-mail.</i>",

    logged: null, // Trenutno ulogovan korisnik.
    loggedBackup: null,

    postListForHome: null, // Lista objava za homepage trenutnog korisnika.

    searchResultsList: null, // Lista korisnika koji su rezultati pretrage.

    selectedProfile: null, // Profil koji je selektovan pretragom.

    profilePhotoMap: null, // Mapa gde je username svakog korisnika mapiran na odgovarajucu profilnu fotografiju.

    newPostToggle: null, // Toggle za prikaz prompta za objavu posta.
    newImgToggle: null, // Toggle za prikaz prompta za objavu fotografije.
    editDataToggle: null,

    maxDate: null,
    nameError: "",
    surnameError: "",
    emailError: "",
    passwordError: "",
    confirmPasswordError: "",

    newPassword: "",
    newPasswordConfirm: "",

    images: [],
    selectedImage: "",

    profileToggle: null, //Toggle za prikaz selected Profile-a.
    profileNavbarToggle: null, // Toggle za my_profile ikonu u navbaru.
    homeNavbarToggle: null, //Toggle za home ikonu u navbaru.
    showNewPostToggle: null, //Toggle za prikaz dugmadi za nove objave i slike.
    showEditToggle: null, // Toggle za prikaz dugmeta za editovanje profila.
    showPostsToggle: null, // Toggle za prikaz objava
    showSearchToggle: null, // Toggle za prikaz rezultata pretrage
    showRequestsToggle: null, // Toggle za prikaz zahteva za prijateljstvo
    showMessagesToggle: null, // Toggle za prikaz poruka.

    //---------------
    admins: ["jovansa", "djosa"],

    selectedRecipient: null, // Profil koji je selektovan za slanje poruka

    messages: [],
    chatSocket: null,
    message: null,
    friendToBlink: null,
    msgList: null,

    canSeeToggle: true
  },
  created() {
    axios
      .get("/getuser", { params: { message: "ok" } })
      .then((response) => {
        let res = response.data;
        if (res.status === "success") {
          this.logged = res.info;
          this.loggedBackup = JSON.parse(JSON.stringify(this.logged));
          this.selectedImage = this.logged.profilePic;
          for (let i = 0; i < this.logged.posts.length; i++) {
            if (this.logged.posts[i].type == "IMAGE") {
              this.images.push(this.logged.posts[i]);
            }
          }
          try {
            let dateOfBirth = this.logged.dateOfBirth.split(".");
            dateOfBirth =
              dateOfBirth[2] + "-" + dateOfBirth[1] + "-" + dateOfBirth[0];
            this.logged.dateOfBirth = dateOfBirth;
            this.loggedBackup.dateOfBirth = dateOfBirth;
          } catch (error) {
            let dateOfBirth = "1970-01-01";
            this.logged.dateOfBirth = dateOfBirth;
            this.loggedBackup.dateOfBirth = null;
          }

          let today = new Date();
          let dd = today.getDate();
          let mm = today.getMonth() + 1;
          let yyyy = today.getFullYear();

          if (dd < 10) {
            dd = "0" + dd;
          }

          if (mm < 10) {
            mm = "0" + mm;
          }
          this.maxDate = yyyy + "-" + mm + "-" + dd;

          this.newPostToggle = true;
          this.newImgToggle = true;
          this.editDataToggle = true;
          this.profileToggle = false;
          this.selectedProfile = res.info;
          this.homeNavbarToggle = false;
          this.profileNavbarToggle = true;
          this.showNewPostToggle = true;
          this.showEditToggle = false;
          this.showSearchToggle = false;
          this.showPostsToggle = true;
          this.searchResultsList = [];
          this.showRequestsToggle = false;
          this.showMessagesToggle = false;
          this.selectedRecipient = "";
          this.msgList = this.logged.friendList.concat();
        }
      })
      .catch((error) => {
        if (error.response) {
          let res = error.response.data;
          if (res.info === "invalid request") {
            alert(res.info);
          }
        }
      });
    axios
      .get("/getHomePosts", { params: { message: "ok" } })
      .then((response) => {
        var res = response.data;
        if (res.status === "success") {
          this.postListForHome = res.info;
        }
      })
      .catch((error) => {
        if (error.response) {
          let res = error.response.data;
          if (res.info === "invalid request") {
            alert(res.info);
          }
        }
      });

    axios
      .get("/getProfilePhotoMap", { params: { message: "ok" } })
      .then((response) => {
        var res = response.data;
        if (res.status === "success") {
          this.profilePhotoMap = res.info;
        }
      })
      .catch((error) => {
        if (error.response) {
          let res = error.response.data;
          if (res.info === "invalid request") {
            alert(res.info);
          }
        }
      });
  },
  mounted() {
    this.chatSocket = new WebSocket(
      "ws://" + location.hostname + ":" + location.port + "/chat"
    );

    this.chatSocket.onmessage = (e) => {
      const data = JSON.parse(e.data);
      this.messages.push(data);
      let icon = document.getElementById("inbox-icon");
      if (this.showMessagesToggle === false) {
        icon.classList.add("blink");
        icon.classList.add("new-msg");
      }
      this.friendToBlink = data.sender;
    };

    this.chatSocket.onclose = (e) => {
      alert("Websocket connection lost!");
      return;
    };
  },
  methods: {
    sendMessage: function () {
      if (this.message != "" && this.message != null) {
        axios
          .post("/newMsg", {
            id: this.selectedRecipient,
            message: "ok:" + this.message
          })
          .then((response) => {
            let res = response.data;
            if (res.status === "success") {
              this.messages.push(res.info);
              this.chatSocket.send(JSON.stringify(res.info));
              this.message = "";
              return;
            }
          })
          .catch((error) => {
            if (error.response) {
              let res = error.response.data;
              if (res.info === "invalid request") {
                alert(res.info);
              }
            }
          });
      }
    },
    logout: () => {
      axios
        .get("/logout")
        .then((response) => {
          let res = response.data;
          if (res.status === "success") {
            window.location.href = "/";
          }
        })
        .catch(() => {
          alert("Invalid request!");
        });
    },
    newPost: function (e) {
      let proceed = false;
      let img = document.querySelector("#post-post");
      let txt = document.querySelector("#post-post-text").value;
      const validImageTypes = ["image/jpg", "image/jpeg", "image/png"];
      try {
        if (
          (validImageTypes.includes(img.files[0]["type"]) ||
            img.files.length === 0) &&
          txt != ""
        ) {
          proceed = true;
        } else {
          if (txt == "") alert("Post must contain some text!");
          else alert("Invalid file type!");
          e.preventDefault();
        }
      } catch (error) {
        if (txt != "") proceed = true;
        else {
          alert("Post must contain some text!");
          e.preventDefault();
        }
      }
      if (proceed) {
        let file = img.files[0];
        let formData = new FormData();
        formData.append("file", file);
        axios
          .post("/newPost", formData, {
            headers: {
              "Content-Type": "multipart/form-data"
            },
            params: { text: txt, type: "post" }
          })
          .then((response) => {
            axios
              .get("/getuser", { params: { message: "ok" } })
              .then((response) => {
                let res = response.data;
                if (res.status === "success") {
                  this.logged = res.info;
                  this.postListForHome.unshift(
                    this.logged.posts[this.logged.posts.length - 1]
                  );
                  this.newPostToggle = !this.newPostToggle;
                  alert("Post added successfully!");
                }
              })
              .catch((error) => {
                if (error.response) {
                  let res = error.response.data;
                  if (res.info === "invalid request") {
                    alert(res.info);
                  }
                }
              });
          });
      }
    },
    newImg: function (e) {
      let proceed = false;
      let img = document.querySelector("#post-image");
      let txt = document.querySelector("#post-image-text").value;
      const validImageTypes = ["image/jpg", "image/jpeg", "image/png"];
      try {
        if (validImageTypes.includes(img.files[0]["type"])) {
          proceed = true;
        } else {
          alert("Invalid file type!");
          e.preventDefault();
        }
      } catch (error) {
        if (img.files.length == 0) {
          alert("Image post must contain an image!");
          e.preventDefault();
        }
      }
      if (proceed) {
        let file = img.files[0];
        let formData = new FormData();
        formData.append("file", file);
        axios
          .post("/newPost", formData, {
            headers: {
              "Content-Type": "multipart/form-data"
            },
            params: { text: txt, type: "image" }
          })
          .then((response) => {
            axios
              .get("/getuser", { params: { message: "ok" } })
              .then((response) => {
                let res = response.data;
                if (res.status === "success") {
                  this.logged = res.info;
                  this.postListForHome.unshift(
                    this.logged.posts[this.logged.posts.length - 1]
                  );
                  this.newImgToggle = !this.newImgToggle;
                  alert("Post added successfully!");
                }
              })
              .catch((error) => {
                if (error.response) {
                  let res = error.response.data;
                  if (res.info === "invalid request") {
                    alert(res.info);
                  }
                }
              });
          });
      }
    },
    reveal: (id) => {
      var img = document.getElementById("img" + id);
      var comments = document.getElementById("comments" + id);
      var button = document.getElementById("button" + id);
      if (img != null) {
        if (img.style.display === "none") {
          img.style.display = "block";
          button.innerHTML = "Hide Details";
        } else {
          img.style.display = "none";
          button.innerHTML = "Show Details";
        }
      }
      if (comments != null) {
        if (comments.style.display === "none") {
          comments.style.display = "block";
          button.innerHTML = "Hide Details";
        } else {
          comments.style.display = "none";
          button.innerHTML = "Show Details";
        }
      }
    },
    deletePost: function (id, author) {
      if (confirm("Are you sure you want to delete this post?")) {
        axios
          .put("/deletePost", { id: id + ":" + author, message: "ok" })
          .then((response) => {
            let res = response.data;
            if (res.status === "success") {
              let index = 0;
              for (let elem of this.postListForHome) {
                if (elem.id === id) {
                  break;
                }
                index++;
              }
              this.postListForHome[index].isDeleted = true;
              for (let com of this.postListForHome[index].comments)
                com.isDeleted = true;
            }
          })
          .catch((error) => {
            if (error.response) {
              let res = error.response.data;
              if (res.info === "invalid request") {
                alert(res.info);
              }
            }
          });
      }
    },
    postComment: function (id) {
      let commentText = document.getElementById("post-comment" + id).value;
      if (commentText != "") {
        axios
          .post("/newComment", { id: id + ":" + commentText, message: "ok" })
          .then((response) => {
            let res = response.data;
            if (res.status === "success") {
              for (let p of this.postListForHome) {
                if (p.id === id) {
                  p.comments.push(res.info);
                  document.getElementById("post-comment" + id).value = "";
                  return;
                }
              }
            }
          })
          .catch((error) => {
            if (error.response) {
              let res = error.response.data;
              if (res.info === "invalid request") {
                alert(res.info);
              }
            }
          });
      }
    },
    deleteComment: function (id) {
      if (confirm("Are you sure you want to delete this comment?")) {
        axios
          .put("/deleteComment", { id: id, message: "ok" })
          .then((response) => {
            let res = response.data;
            if (res.status === "success") {
              for (let p of this.postListForHome) {
                for (let c of p.comments) {
                  if (c.id === id) {
                    c.isDeleted = true;
                    return;
                  }
                }
              }
            }
          })
          .catch((error) => {
            if (error.response) {
              let res = error.response.data;
              if (res.info === "invalid request") {
                alert(res.info);
              }
            }
          });
      }
    },
    editComment: function (id) {
      let comment = document.getElementById("commentText" + id);
      let commentText = comment.innerHTML;
      let editedText = window.prompt("Edit comment", commentText);
      if (
        editedText == null ||
        editedText == "" ||
        editedText === commentText
      ) {
        comment.innerHTML = commentText;
        return;
      } else {
        axios
          .put("/editComment", { id: id + ":" + editedText, message: "ok" })
          .then((response) => {
            let res = response.data;
            if (res.status === "success") {
              for (let p of this.postListForHome) {
                for (let c of p.comments) {
                  if (c.id === id) {
                    c.text = editedText;
                    let now = getDateString(new Date());
                    c.lastModified = now;
                    return;
                  }
                }
              }
            }
          })
          .catch((error) => {
            if (error.response) {
              let res = error.response.data;
              if (res.info === "invalid request") {
                alert(res.info);
              }
            }
          });
      }
    },
    isFriend: function (username) {
      if (this.logged.friendList.includes(username)) return true;
      return false;
    },
    edit: function (e) {
      let allGood = true;
      if (this.logged.email != this.loggedBackup.email) {
        if (!/\S+@\S+\.\S+/.test(this.logged.email)) {
          this.emailError = "Invalid e-mail format!";
          allGood = false;
          e.preventDefault();
        }
      }
      if (this.newPassword != "") {
        if (this.newPassword.length < 5 || this.newPassword.length > 15) {
          this.passwordError = "Password length must be 5-15!";
          allGood = false;
          e.preventDefault();
        }
        if (this.newPassword !== this.newPasswordConfirm) {
          this.confirmPasswordError = "Passwords do not match!";
          allGood = false;
          e.preventDefault();
        }
        this.logged.password = this.newPassword;
      }
      if (this.logged.name != this.loggedBackup.name) {
        if (!/^[A-Za-z]+$/.test(this.logged.name)) {
          this.nameError = "Invalid name!";
          allGood = false;
          e.preventDefault();
        }
      }

      if (this.logged.surname != this.loggedBackup.surname) {
        if (!/^[A-Za-z]+$/.test(this.logged.surname)) {
          this.surnameError = "Invalid surname!";
          allGood = false;
          e.preventDefault();
        }
      }

      if (this.selectedImage !== this.logged.profilePic) {
        this.logged.profilePic = this.selectedImage;
        this.profilePhotoMap[this.logged.username] = this.selectedImage;
      }

      if (allGood) {
        let dob =
          this.logged.dateOfBirth === ""
            ? this.loggedBackup.dateOfBirth
            : this.logged.dateOfBirth;
        let dobParts = dob.split("-");
        let serverDateFormat =
          dobParts[2] + "." + dobParts[1] + "." + dobParts[0] + ".";
        this.logged.dateOfBirth = serverDateFormat;
        axios
          .post("/edit", this.logged)
          .then((response) => {
            let res = response.data;
            if (res.status === "success") {
              this.emailError = "";
              this.passwordError = "";
              this.confirmPasswordError = "";
              this.nameError = "";
              this.surnameError = "";
              this.logged = JSON.parse(response.config.data);
              this.logged.dateOfBirth = JSDobFormat(this.logged.dateOfBirth);
              this.loggedBackup = JSON.parse(JSON.stringify(this.logged));
              this.selectedImage = this.logged.profilePic;
              this.newPassword = "";
              this.newPasswordConfirm = "";
              alert("Personal data has been edited successfully!");
            }
          })
          .catch((error) => {
            if (error.response) {
              alert(error.response.data.info);
            }
          });
      }
    },
    deleteError: function (error) {
      if (error === "Invalid e-mail format!") this.emailError = "";
      else if (error === "Passwords do not match!")
        this.confirmPasswordError = "";
      else if (error === "Password length must be 5-15!")
        this.passwordError = "";
      else if (error === "Invalid name!") this.nameError = "";
      else if (error === "Invalid surname!") this.surnameError = "";
      else if (error === "all") {
        this.emailError = "";
        this.confirmPasswordError = "";
        this.passwordError = "";
        this.nameError = "";
        this.surnameError = "";
        this.newPassword = "";
        this.newPasswordConfirm = "";
      }
    },

    selectImage: function (img) {
      this.selectedImage = img.image;
    },

    selectMyProfile: function () {
      this.selectedProfile = this.logged;
      this.postListForHome = this.logged.posts;
      this.profileToggle = true;
      this.homeNavbarToggle = true;
      this.profileNavbarToggle = false;
      this.showNewPostToggle = true;
      this.showEditToggle = true;
      this.editDataToggle = true;
      this.showSearchToggle = false;
      this.showPostsToggle = true;
      this.showRequestsToggle = false;
      this.showMessagesToggle = false;
    },
    goBackHome: function () {
      axios
        .get("/getHomePosts", { params: { message: "ok" } })
        .then((response) => {
          var res = response.data;
          if (res.status === "success") {
            this.postListForHome = res.info;
          }
        })
        .catch((error) => {
          if (error.response) {
            let res = error.response.data;
            if (res.info === "invalid request") {
              alert(res.info);
            }
          }
        });
      this.homeNavbarToggle = false;
      this.profileToggle = false;
      this.profileNavbarToggle = true;
      this.showNewPostToggle = true;
      this.showEditToggle = false;
      this.showSearchToggle = false;
      this.showPostsToggle = true;
      this.showRequestsToggle = false;
      this.showMessagesToggle = false;
    },
    loadFriend: function (friend) {
      this.profileToggle = true;
      this.profileNavbarToggle = true;
      this.showNewPostToggle = false;
      this.showEditToggle = false;
      this.showSearchToggle = false;
      this.showPostsToggle = true;
      this.showRequestsToggle = false;
      this.showMessagesToggle = false;
      axios
        .get("/getSelectedUser", {
          params: { message: "ok", username: friend }
        })
        .then((response) => {
          let res = response.data;
          if (res.status === "success") {
            this.selectedProfile = res.info;
            this.postListForHome = res.info.posts;
            this.canSeeToggle =
              !this.selectedProfile.isPrivate ||
              this.logged.role === "ADMINISTRATOR" || this.logged.friendList.includes(this.selectedProfile.username);
            this.homeNavbarToggle = true;
            if (this.selectedProfile.username === this.logged.username)
              this.profileNavbarToggle = false;
          }
        });
    },
    editToggler: function (isToggled) {
      if (isToggled === false) this.editDataToggle = true;
      this.editDataToggle = !this.editDataToggle;
      this.newPostToggle = true;
      this.newImgToggle = true;
    },
    showRequests: function () {
      this.profileToggle = false;
      this.profileNavbarToggle = true;
      this.homeNavbarToggle = true;
      this.showNewPostToggle = false;
      this.showEditToggle = false;
      this.showPostsToggle = false;
      this.showSearchToggle = false;
      this.showRequestsToggle = true;
      this.showMessagesToggle = false;
    },
    showMessages: function () {
      this.profileToggle = false;
      this.profileNavbarToggle = true;
      this.homeNavbarToggle = true;
      this.showNewPostToggle = false;
      this.showEditToggle = false;
      this.showPostsToggle = false;
      this.showSearchToggle = false;
      this.showRequestsToggle = false;
      this.showMessagesToggle = true;

      let icon = document.getElementById("inbox-icon");
      if (icon.classList.contains("blink")) icon.classList.remove("blink");
      if (icon.classList.contains("new-msg")) icon.classList.remove("new-msg");

      this.messageList();
    },
    search: function () {
      this.profileToggle = false;
      this.profileNavbarToggle = true;
      this.homeNavbarToggle = true;
      this.showNewPostToggle = false;
      this.showEditToggle = false;
      this.showPostsToggle = false;
      this.showSearchToggle = true;
      this.showRequestsToggle = false;
      this.showMessagesToggle = false;

      let searchString = document.getElementById("search-name").value;
      let dateQuery = document.getElementById("range-picker").value;

      let searchParams = searchString.split(" ");
      let dateParams = extractDatesFromRange(dateQuery);

      let isAdmin = this.logged.role === "ADMINISTRATOR";

      axios
        .get("/getSearchResults", {
          params: {
            message: "ok",
            searchParams: JSON.stringify(searchParams),
            dateParams: JSON.stringify(dateParams),
            isAdmin: isAdmin
          }
        })
        .then((response) => {
          let res = response.data;
          if (res.status === "success") {
            if (this.logged.role === "GUEST") {
              this.searchResultsList = res.info.filter(
                (user) =>
                  !user.isPrivate ||
                  this.logged.friendList.includes(user.username)
              );
            } else {
              this.searchResultsList = res.info;
            }
          }
        });
    },
    confirmRequest: function (id, confirm) {
      var sender;
      for (let req of this.logged.friendRequests) {
        if (req.id === id) {
          if (confirm) req.status = "ACCEPTED";
          else req.status = "DECLINED";
          sender = req.sender;
          break;
        }
      }
      axios
        .put("/modifyFriendRequest", { id: id + ":" + confirm, message: "ok" })
        .then((response) => {
          let res = response.data;
          if (res.status === "success") {
            this.logged.friendList.push(sender);
          }
        })
        .catch((error) => {
          if (error.response) {
            let res = error.response.data;
            if (res.info === "invalid request") {
              alert(res.info);
            }
          }
        });
    },
    asc: function (property) {
      this.searchResultsList.sort((a, b) =>
        a[property] > b[property] ? 1 : b[property] > a[property] ? -1 : 0
      );
    },
    desc: function (property) {
      this.asc(property);
      this.searchResultsList.reverse();
    },
    dobAsc: function () {
      let noDob = this.searchResultsList.filter(
        (user) => !user.hasOwnProperty("dateOfBirth")
      );

      let yesDob = this.searchResultsList.filter((user) =>
        user.hasOwnProperty("dateOfBirth")
      );

      yesDob.sort((a, b) =>
        Date.parse(JSDobFormat(a["dateOfBirth"])) >
        Date.parse(JSDobFormat(b["dateOfBirth"]))
          ? -1
          : Date.parse(JSDobFormat(a["dateOfBirth"])) <
            Date.parse(JSDobFormat(b["dateOfBirth"]))
          ? 1
          : 0
      );
      this.searchResultsList = yesDob.concat(noDob);
    },
    dobDesc: function () {
      let noDob = this.searchResultsList.filter(
        (user) => !user.hasOwnProperty("dateOfBirth")
      );

      let yesDob = this.searchResultsList.filter((user) =>
        user.hasOwnProperty("dateOfBirth")
      );

      yesDob.sort((a, b) =>
        Date.parse(JSDobFormat(a["dateOfBirth"])) >
        Date.parse(JSDobFormat(b["dateOfBirth"]))
          ? 1
          : Date.parse(JSDobFormat(a["dateOfBirth"])) <
            Date.parse(JSDobFormat(b["dateOfBirth"]))
          ? -1
          : 0
      );
      this.searchResultsList = yesDob.concat(noDob);
    },
    getMutualFriends: function () {
      return this.logged.friendList.filter((value) =>
        this.selectedProfile.friendList.includes(value)
      );
    },
    getMutualFriendsLen: function () {
      return this.logged.friendList.filter((value) =>
        this.selectedProfile.friendList.includes(value)
      ).length;
    },
    removeFriend: function (username) {
      let index = this.logged.friendList.indexOf(username);
      let index2 = this.selectedProfile.friendList.indexOf(
        this.logged.username
      );
      this.selectedProfile.friendList.splice(index2, 1);
      this.logged.friendList.splice(index, 1);
      axios
        .put("/removeFriend", { id: username, message: "ok" })
        .then((response) => {
          let res = response.data;
          if (res.status === "success") {
            return;
          }
        })
        .catch((error) => {
          if (error.response) {
            let res = error.response.data;
            if (res.info === "invalid request") {
              alert(res.info);
            }
          }
        });
    },
    addFriend: function (username) {
      axios
        .post("/addFriend", { id: username, message: "ok" })
        .then((response) => {
          let res = response.data;
          if (res.status === "success") {
            this.selectedProfile.friendRequests.push(res.info);
            return;
          }
        })
        .catch((error) => {
          if (error.response) {
            let res = error.response.data;
            if (res.info === "invalid request") {
              alert(res.info);
            }
          }
        });
    },
    selectForMessaging: function (friend) {
      this.messages = [];
      this.selectedRecipient = friend;
      axios
        .get("/getMessages", { params: { message: "ok", target: friend } })
        .then((response) => {
          var res = response.data;
          if (res.status === "success") {
            this.messages = res.info;
          }
        })
        .catch((error) => {
          if (error.response) {
            let res = error.response.data;
            if (res.info === "invalid request") {
              alert(res.info);
            }
          }
        });
    },
    directMessageButton: function (target) {
      this.showMessages();
      this.selectForMessaging(target);
    },
    blockUser: function (username) {
      let button = document.getElementById("block-button" + username);
      if (button.classList.contains("btn-danger")) {
        button.classList.remove("btn-danger");
        button.classList.add("btn-success");
        button.innerHTML =
          "Unblock<i class='fas fa-lock-open block-button-icon'></i>";
        axios.get("/blockUser", {
          params: { message: "ok", username: username, block: true }
        });
      } else {
        button.classList.add("btn-danger");
        button.classList.remove("btn-success");
        button.innerHTML = "Block<i class='fas fa-lock block-button-icon'></i>";
        axios
          .get("/blockUser", {
            params: { message: "ok", username: username, block: false }
          })
          .catch((error) => {
            if (error.response) {
              let res = error.response.data;
              if (res.info === "invalid request") {
                alert(res.info);
              }
            }
          });
      }
    },
    blockClass: function (user) {
      return {
        "btn-danger": !user.isBlocked,
        "btn-success": user.isBlocked,
        "fa-lock": !user.isBlocked,
        "fa-lock-open": user.isBlocked
      };
    },
    blockString: function (user) {
      return user.isBlocked ? "Unblock" : "Block";
    },
    adminDeletePost: function (id, author) {
      this.deletePost(id, author);
      let msg = prompt("Please state the reason for deletion.");
      this.selectedRecipient = author;
      this.message =
        "WARNING! Your post (id = " +
        id +
        ") has been deleted! Reason - '" +
        msg +
        "'.";
      this.sendMessage();
      this.selectedRecipient = "";
    },
    messageList: function () {
      let retVal = [];
      axios
        .get("/hasMsgWithAdmin", {
          params: { message: "ok" }
        })
        .then((response) => {
          let res = response.data;
          if (res.status === "success") {
			this.msgList = this.msgList.filter( ( el ) => !response.data.info.includes( el ) );
            this.msgList.push(...response.data.info);
            return;
          }
        })
        .catch((error) => {
          if (error.response) {
            let res = error.response.data;
            if (res.info === "invalid request") {
              alert(res.info);
            }
          }
        });
    }
  },
  filters: {
    appendSuffix: (value) => {
      return value + "'s profile";
    },
    cut: (value) => {
      let tokens = value.split(" ");
      let jsformat = JSDobFormat(tokens[0]);
      let date = new Date(jsformat);
      let today = new Date();
      if (date < today) return value.substring(0, value.length - 3);
      else return value.substring(11, value.length - 3);
    }
  },
  computed: {
    mutualFriendsLen: function () {
      return this.logged.friendList.filter((value) =>
        this.selectedProfile.friendList.includes(value)
      ).length;
    },
    mutualFriends: function () {
      return this.logged.friendList.filter((value) =>
        this.selectedProfile.friendList.includes(value)
      );
    },
    friendListLabel: function () {
      return this.logged.username === this.selectedProfile.username
        ? "Friend List"
        : "Mutual Friends";
    },
    emptyFriendListLabel: function () {
      return this.logged.username === this.selectedProfile.username
        ? "You have no friends!"
        : "You have no mutual friends!";
    },
    reformattedDob: function () {
      try {
        if (this.selectedProfile.dateOfBirth.split(".").length < 3) {
          let tokens = this.selectedProfile.dateOfBirth.split("-");
          return tokens[2] + "." + tokens[1] + "." + tokens[0] + ".";
        } else return this.selectedProfile.dateOfBirth;
      } catch (error) {
        return "01.01.1970.";
      }
    },
    pendingRequestsLength: function () {
      let retVal = 0;
      for (let req of this.logged.friendRequests) {
        if (req.status === "PENDING") retVal++;
      }
      return retVal;
    },
    isPending: function () {
      if (!this.isFriend(this.selectedProfile.username)) {
        for (let req of this.selectedProfile.friendRequests) {
          if (req.sender === this.logged.username && req.status === "PENDING")
            return true;
        }
      }
      return false;
    },
    isAdmin: function () {
      return this.logged.role === "ADMINISTRATOR";
    },
    canSee: function () {
      return this.canSeeToggle;
    }
  }
});

$(function () {
  $('input[name="daterange"]').daterangepicker(
    {
      opens: "left",
      maxDate: getFormatDate(new Date())
    },
    function (start, end) {
      console.log(
        "A new date selection was made: " +
          start.format("YYYY-MM-DD") +
          " to " +
          end.format("YYYY-MM-DD")
      );
    }
  );
});

function getDateString(date) {
  let month =
    date.getMonth() + 1 < 10
      ? "0" + (date.getMonth() + 1)
      : date.getMonth() + 1;
  let day = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
  let hour = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
  let minute =
    date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
  let second =
    date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();

  return (
    day +
    "." +
    month +
    "." +
    date.getFullYear() +
    " " +
    hour +
    ":" +
    minute +
    ":" +
    second
  );
}

function getFormatDate(d) {
  return d.getMonth() + 1 + "/" + d.getDate() + "/" + d.getFullYear();
}

$(function () {
  $('[data-toggle="tooltip"]').tooltip();
});

function extractDatesFromRange(range) {
  range = range.replaceAll(" ", "");
  let twoDates = range.split("-");
  let startDateTokens = twoDates[0].split("/");
  let endDateTokens = twoDates[1].split("/");

  let date1 =
    startDateTokens[1] +
    "." +
    startDateTokens[0] +
    "." +
    startDateTokens[2] +
    ".";
  let date2 =
    endDateTokens[1] + "." + endDateTokens[0] + "." + endDateTokens[2] + ".";

  return [date1, date2];
}

function JSDobFormat(date) {
  let tokens = date.split(".");
  return tokens[2] + "-" + tokens[1] + "-" + tokens[0];
}
