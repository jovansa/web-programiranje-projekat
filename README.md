# Connectify - Web Programiranje

Projektni zadatak iz kursa "Web programiranje".
Tim čine:  
+ Jovan Tomić (SW 48/2019)  
+ Đorđe Jovanović (SW 42/2019)  

*Rok za predaju je 5.2.2022.*

## Opis projekta

Potrebno je realizovati veb aplikaciju koja korisnicima omogućava da:
+ Postavljaju slike na svoj profil
+ Dele objave sa ili bez slika
+ Komentarišu i reaguju na druge objave ili slike 
+ Razmenjuju direktne poruke[^1] 

Aplikacija razlikuje 2 vrste registrovanih korisnika:
+ Administrator
+ Regularni korisnik

Pojedine funkcionalnosti su omogućene i neregistrovanim posetiocima. *Detaljna specifikacija je dostupna u PDF dokumentu.*

[^1]: Razmenu direktnih poruka poželjno bi bilo implementirati u realnom vremenu. 
## Korišćene tehnologije

Postoji određeni skup tehnologija koje su dozvoljene za korišćenje (HTML, CSS, JS, Java), kao i određeni razvojni okviri. Tehnologije koje su korišćene: JavaSpark za backend, Vue.js za frontend (i jQuery, ali veoma malo).

---
