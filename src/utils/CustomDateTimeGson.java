package utils;

import java.time.LocalDate;
import java.time.LocalDateTime;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * This class represents a container for a modified Gson object which makes it 
 * possible to serialize LocalDate and LocalDateTime objects into strings with 
 * custom formats.
 * 
 * @see utils.LocalDateSerializer
 * @see utils.LocalDateDeserializer
 * @see utils.LocalDateTimeSerializer
 * @see utils.LocalDateTimeDeserializer
 * 
 * @author jovansa
 *
 */
public class CustomDateTimeGson {

	private Gson gson;
	
	public CustomDateTimeGson() {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(LocalDateTime.class, new LocalDateTimeSerializer());
		gsonBuilder.registerTypeAdapter(LocalDateTime.class, new LocalDateTimeDeserializer());
		gsonBuilder.registerTypeAdapter(LocalDate.class, new LocalDateSerializer());
		gsonBuilder.registerTypeAdapter(LocalDate.class, new LocalDateDeserializer());
		this.gson = gsonBuilder.setPrettyPrinting().create();
	}
	
	public Gson getCustomGson() {
		return this.gson;
	}
	
}
