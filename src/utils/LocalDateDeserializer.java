package utils;

import java.lang.reflect.Type;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

/**
 * This is a custom deserializer which makes it possible to deserialize LocalDate
 * types from JSON with a custom date format.
 * 
 * e.g. 
 * 
 * 	"date" : "01.01.2000."
 * 
 * instead of 
 * 	
 * 	"date" : { "year": 2000, "month": 1, "day": 1}
 * 
 * @author Ramesh Fadatare (www.javaguides.net)
 *
 */
public class LocalDateDeserializer implements JsonDeserializer < LocalDate > {
    
	@Override
    public LocalDate deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
    throws JsonParseException {
        return LocalDate.parse(json.getAsString(),
            DateTimeFormatter.ofPattern("dd.MM.yyyy."));
    }
	
}
