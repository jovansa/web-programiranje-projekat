package utils;

import java.util.regex.Pattern;

import beans.SignupUser;

/**
 * This is the UserValidator class, which is used for server side sign up
 * validation.
 * 
 * Every instance of this class is bound to one SignupUser object at a time.
 * 
 * @author jovansa
 *
 */

public class UserValidator {

	private SignupUser user;
	private boolean valid;
	
	public UserValidator(SignupUser user) {
		this.user = user;
		this.valid = validateUser();
	}
	
	public boolean isValid() {
		return this.valid;
	}
	
	/**
	 * This method is used to determine whether the user which was constructed
	 * from content sent by client is valid. Since it is only possible for this
	 * validation to fail if the client disables scripts or otherwise interferes
	 * with client side validation, there is no detailed response.
	 * 
	 * @return JSON containing validation information.
	 *     
	 *     required fields in JSON: "status" : "error" or "status" : "success"
	 *     optional fields in JSON: "info" : "error description"
	 * 
	 */
	
	public String getValidationJSON() {
		if(this.valid == true)
			return "{\"status\": \"success\"}";
		return "{\"status\": \"error\", \"info\": \"invalid sign up data\"}";
	}
	
	/**
	 * @return true if sign up input is valid, false otherwise
	 */
	private boolean validateUser() {
		if (user.getSignupUsername().length() < 5 ||
				user.getSignupUsername().length() > 15)
			return false;
		if (!Pattern.matches("[a-zA-Z0-9_]+", user.getSignupUsername()))
			return false;
		if (!Pattern.matches("\\S+@\\S+\\.\\S+", user.getEmail()))
			return false;
		if (user.getSignupPassword().length() < 5 ||
				user.getSignupPassword().length() > 15)
			return false;
		if (!user.getSignupPassword().equals(user.getConfirmPassword()))
			return false;
		if (!Pattern.matches("[A-Za-z]+", user.getName()))
			return false;
		if (!Pattern.matches("[A-Za-z]+", user.getSurname()))
			return false;
		return true;
	}	
}
