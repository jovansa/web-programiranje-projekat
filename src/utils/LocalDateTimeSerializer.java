package utils;

import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * This is a custom serializer which makes it possible to serialize LocalDateTime
 * into JSON with a given custom DateTimeFormatter.
 * 
 * e.g. 
 * 
 * 	"date" : "01.01.2000. 10:10:10"
 * 
 * instead of 
 * 	
 * 	{"date":{"year":2000,"month":1,"day":1},"time":{"hour":10,"minute":10,"second":10,"nano":0}}
 * 
 * @author Ramesh Fadatare (www.javaguides.net)
 *
 */
public class LocalDateTimeSerializer implements JsonSerializer <LocalDateTime> {
    
	private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy. HH:mm:ss");

    @Override
    public JsonElement serialize(LocalDateTime localDateTime, Type srcType, JsonSerializationContext context) {
        return new JsonPrimitive(formatter.format(localDateTime));
    }
    
}
