package utils;

import java.lang.reflect.Type;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * This is a custom serializer which makes it possible to serialize LocalDate
 * into JSON with a given custom DateTimeFormatter.
 * 
 * e.g. 
 * 
 * 	"date" : "01.01.2000."
 * 
 * instead of 
 * 	
 * 	"date" : { "year": 2000, "month": 1, "day": 1}
 * 
 * @author Ramesh Fadatare (www.javaguides.net)
 *
 */
public class LocalDateSerializer implements JsonSerializer <LocalDate> {
    
	private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy.");

    @Override
    public JsonElement serialize(LocalDate localDate, Type srcType, JsonSerializationContext context) {
        return new JsonPrimitive(formatter.format(localDate));
    }
    
}
