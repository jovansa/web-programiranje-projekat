package enums;

/**
 * This is the Gender enumeration. It does not intend to offend anyone who
 * doesn't recognize themselves with any of the offered options.
 * 
 * @author jovansa
 * 
 */
public enum Gender {
	MALE,
	FEMALE
}
