package enums;

/**
 * This is the Role enumeration. It is used to determine authorizations of a user.
 * 
 * 
 * @author jovansa
 * 
 */
public enum Role {
	ADMINISTRATOR,
	USER,
	GUEST
}
