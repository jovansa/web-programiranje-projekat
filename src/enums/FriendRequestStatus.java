package enums;

/**
 * This is the FriendRequsetStatus enumeration. It is used to describe the
 * current status of a friend request.
 * 
 * @author jovansa
 *
 */
public enum FriendRequestStatus {
	ACCEPTED,
	PENDING,
	DECLINED
}
