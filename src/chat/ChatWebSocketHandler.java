package chat;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.*;

import com.google.gson.Gson;

import beans.DirectMessage;
import main.Application;
import utils.CustomDateTimeGson;

@WebSocket
public class ChatWebSocketHandler {
	
	private static ConcurrentHashMap<Session, String> userMap = new ConcurrentHashMap<Session, String>();
	private static Gson g = new CustomDateTimeGson().getCustomGson();
	
	public static void broadcastMessage(String recipient, String message) {
        for(Map.Entry<Session, String> entry: userMap.entrySet()) {
        	if(entry.getKey().isOpen() && entry.getValue().equals(recipient)) {
				try {
					entry.getKey().getRemote().sendString(message);
				} catch (IOException e) {
					e.printStackTrace();
				}
        	}
        }
    }
	
	@OnWebSocketConnect
	public void onConnect(Session user) throws Exception{
		try {
			userMap.put(user, Application.username);
		} catch (NullPointerException npe) {
			return;
		}
	}
	
	@OnWebSocketClose
	public void onClose(Session user, int statusCode, String reason) throws Exception {
		try {
			userMap.remove(user);
		} catch (NullPointerException npe) {
			return;
		}
	}
	
	@OnWebSocketMessage
	public void onMessage(Session sender, String message) {
		DirectMessage dm = g.fromJson(message, DirectMessage.class);
		broadcastMessage(dm.getRecipient(), g.toJson(dm));
	}
}
