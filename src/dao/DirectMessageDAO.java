package dao;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import beans.DirectMessage;
import utils.CustomDateTimeGson;

/**
 * This is the DirectMessage DAO class which acts as a container for all Direct 
 * Message instances in the application. DM's are loaded from the .json file upon
 * instantiation.
 * 
 * Note: DM = Direct Message.
 * 
 * @author jovansa
 *
 */

public class DirectMessageDAO {

	/**
	 * A hashmap whose keys are DM ID's, and the values are the
	 * corresponding DM instances.
	 */
	private HashMap<String, DirectMessage> messages;
	private static Gson g = new CustomDateTimeGson().getCustomGson();
	
	public static final String path = "data\\json\\messages.json";
	
	public DirectMessageDAO() {
		try {
			loadMessages(path);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public DirectMessage getMessage(String id) {
		if(messageExists(id))
			return messages.get(id);
		return null;
	}
	
	public Collection<DirectMessage> getAllMessages(){
		return messages.values();
	}
	
	public void addMessage(String id, DirectMessage msg) {
		this.messages.put(id, msg);
	}
	
	public Boolean messageExists(String id) {
		return messages.containsKey(id); 
	}
	
	public ArrayList<DirectMessage> getMessagesForTwo(String first, String second){
		ArrayList<DirectMessage> retVal = new ArrayList<DirectMessage>();
		for (DirectMessage msg : messages.values()) {
			if((msg.getSender().equals(first) && msg.getRecipient().equals(second)) ||
				(msg.getSender().equals(second) && msg.getRecipient().equals(first)))
				retVal.add(msg);
		}
		Collections.sort(retVal, new Comparator<DirectMessage>() {
			  public int compare(DirectMessage o1, DirectMessage o2) {
			      return o1.getTimestamp().compareTo(o2.getTimestamp());
			  }
			});
		return retVal;
	}
	
	public String generateDMid() {
		return String.valueOf(messages.values().size() + 1);
	}
	
	public ArrayList<String> adminsWithMsg(UserDAO userDao, String target){
		ArrayList<String> retVal = new ArrayList<String>();
		for(DirectMessage msg: messages.values()) {
			if(msg.getRecipient().equals(target)) {
				if(!userDao.getUser(target).getFriendList().contains(msg.getSender())
						&& !retVal.contains(msg.getSender()))
					retVal.add(msg.getSender());
			}
		}
		return retVal;
	}
	
	/**
	 * @param path - Path to the .json file in which the DM's are stored.
	 * @throws FileNotFoundException 
	 */
	private void loadMessages(String path) throws FileNotFoundException {
		BufferedReader br = new BufferedReader(new FileReader(path));
		Type type = new TypeToken<HashMap<String, DirectMessage>>(){}.getType();
		messages = g.fromJson(br, type);		
	}
	
	public void updateFile() {
		try {
			FileWriter wr = new FileWriter(path);
			g.toJson(messages, wr);
			wr.flush();
			wr.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
