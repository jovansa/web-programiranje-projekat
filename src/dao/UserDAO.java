package dao;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.lang.reflect.Type;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import beans.Post;
import beans.User;
import enums.Role;
import utils.CustomDateTimeGson;
import utils.Pair;

/**
 * This is the UserDAO class which acts as a container for all User instances
 * in the application. Users are loaded from the .json file upon UserDAO class
 * instantiation.
 * 
 * @author jovansa
 *
 */

public class UserDAO {
	
	/**
	 * A hashmap whose keys are User ID's, and the values are the corresponding
	 * user instances.
	 */
	private HashMap<String, User> users;
	static String defaultProfilePicPath = "./WebContent/img/default_profile_pic.jpg";
	
	public static final String path = "data\\json\\users.json";
	
	public UserDAO() {
		try {
			loadUsers(path);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public User getUser(String id) {
		if(userExists(id))
			return users.get(id);
		return null;
	}
	
	public Collection<User> getAllUsers(){
		return users.values();
	}
	
	public void addUser(String id, User user) {
		this.users.put(id, user);
	}
	
	public void replaceUser(String username, User editedUser) {
		users.replace(username, editedUser);
	}
	
	public void updateFile() {
		try {
			FileWriter wr = new FileWriter(path);
			new CustomDateTimeGson().getCustomGson().toJson(users, wr);
			wr.flush();
			wr.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public boolean authenticateLogin(String username, String password) {
		if(userExists(username.toLowerCase())) {
			User u = getUser(username.toLowerCase());
			if (u.getPassword().equals(password) && !u.isBlocked()) 
				return true;
		}
		return false;
	}
	
	public Boolean userExists(String username) {
		return users.containsKey(username); 
	}
	
	/**
	 * @param path - Path to the .json file in which the users are stored.
	 * @throws FileNotFoundException 
	 */
	private void loadUsers(String path) throws FileNotFoundException {
		BufferedReader br = new BufferedReader(new FileReader(path));
		Gson gson = new CustomDateTimeGson().getCustomGson();
		Type type = new TypeToken<HashMap<String, User>>(){}.getType();
		users = gson.fromJson(br, type);		
	}
	
	/**
	 * This function returns a map where each username is mapped to the
	 * respective profile photo.
	 * 
	 * @return Map where key=username, value=profile photo for that user.
	 */
	public HashMap<String, String> getProfilePhotoMap(){
		HashMap<String, String> retVal = new HashMap<String, String>();
		for (User u: users.values())
			retVal.put(u.getUsername(), u.getProfilePic());
		return retVal;
	}
	
	public ArrayList<Post> getPostsForHomepage(User user){
		if(user == null)
			return new ArrayList<Post>();
		ArrayList<Post> retVal = new ArrayList<Post>();
		retVal.addAll(user.getPosts());
		for(String friend : user.getFriendList())
			retVal.addAll(users.get(friend).getPosts());
		Collections.sort(retVal, new Comparator<Post>() {
			  public int compare(Post o1, Post o2) {
			      return o1.getTimestamp().compareTo(o2.getTimestamp());
			  }
			});
		Collections.reverse(retVal);
		retVal.removeIf(p -> p.isDeleted());
		return retVal;
	}
	
	public Post getPostById(String id) {
		for(User u: users.values()) {
			for(Post p: u.getPosts()) {
				if (p.getId().equals(id)) return p;
			}
		}
		return null;
	}
	
	public String generatePostId() {
		int retVal = 1;
		for(User u: users.values()) {
			retVal += u.getPosts().size();
		}
		return String.valueOf(retVal);
	}

	public String generateCommentId() {
		int retVal = 1;
		for(User u: users.values()) {
			for(Post p: u.getPosts())
				retVal += p.getComments().size();
		}
		return String.valueOf(retVal);
	}
	
	public String generateFriendRequestId() {
		int retVal = 1;
		for(User u: users.values())
			retVal += u.getFriendRequests().size();
		return String.valueOf(retVal);
	}
	
	public User getGuestUser() {
		return new User("Guest", "Guest", "Guest", "Guest", "Guest", LocalDate.of(1970, 1, 1), "male", Role.GUEST, defaultProfilePicPath, false);
	}
	
	private int getUserScore(ArrayList<String> user, ArrayList<String> query) {
		int retVal = 0;
		for(String userToken: user) {
			for (String searchToken: query) {
				if(userToken.toLowerCase().contains(searchToken.toLowerCase()))
					retVal += searchToken.length();
			}
		}
		return retVal;
	}
	
	public ArrayList<User> getSearchResult(ArrayList<String> stringQuery, ArrayList<LocalDate> dateQuery, boolean isAdmin){
		ArrayList<User> retVal = new ArrayList<User>();
		boolean ignoreDate = dateQuery.get(0).equals(dateQuery.get(1)) && dateQuery.get(0).equals(LocalDate.now());
		boolean noKeywords = stringQuery.size() == 1 && stringQuery.get(0).equals("");
		ArrayList<Pair<User, Integer>> scoreboard = new ArrayList<Pair<User, Integer>>();
		for (User u: users.values())
			scoreboard.add(Pair.createPair(u, getUserScore(u.getSearchTokens(isAdmin), stringQuery)));
		Collections.sort(scoreboard, new Comparator<Pair<User, Integer>>(){
			@Override
			public int compare(final Pair<User, Integer> o1, final Pair<User, Integer> o2) {
				if(o1.getSecond() > o2.getSecond()) return -1;
				else if (o1.getSecond() < o2.getSecond()) return 1;
				else return 0;
			}
		});
		for(Pair<User, Integer> p : scoreboard) {
			if(noKeywords) {
				if(!ignoreDate) {
					if (p.getFirst().getDateOfBirth().isAfter(dateQuery.get(0).minusDays(1)) &&
					p.getFirst().getDateOfBirth().isBefore(dateQuery.get(1).plusDays(1)))
						retVal.add(p.getFirst());				
				}
				else
					retVal.add(p.getFirst());
			}
			
			else {
				if(p.getSecond() > 0) {
					if(!ignoreDate) {
						if (p.getFirst().getDateOfBirth().isAfter(dateQuery.get(0).minusDays(1)) &&
						p.getFirst().getDateOfBirth().isBefore(dateQuery.get(1).plusDays(1)))
							retVal.add(p.getFirst());				
					}
					else
						retVal.add(p.getFirst());
				}
			}
		}
		return retVal;
	}
}
