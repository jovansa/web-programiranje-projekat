package main;

import static spark.Spark.get;
import static spark.Spark.port;
import static spark.Spark.post;
import static spark.Spark.put;
import static spark.Spark.staticFiles;
import static spark.Spark.webSocket;
import static spark.Spark.init;

import java.io.File;
import java.lang.reflect.Type;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import javax.servlet.MultipartConfigElement;
import javax.servlet.http.Part;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import beans.Comment;
import beans.DirectMessage;
import beans.FriendRequest;
import beans.LoginUser;
import beans.Post;
import beans.RequestBody;
import beans.SignupUser;
import beans.User;
import chat.ChatWebSocketHandler;
import dao.DirectMessageDAO;
import dao.UserDAO;
import enums.FriendRequestStatus;
import enums.PostType;
import enums.Role;
import spark.Session;
import utils.CustomDateTimeGson;
import utils.PostProcessor;
import utils.UserValidator;


public class Application {
	
	private static Gson g = new CustomDateTimeGson().getCustomGson();
	public static UserDAO userDao = new UserDAO();
	private static DirectMessageDAO messageDao = new DirectMessageDAO();
	
	static String path = "./WebContent/";
	static String defaultProfilePicPath = "./WebContent/img/default_profile_pic.jpg";
	
	public static String username;
	
	public static void main(String[] args) throws Exception {
		
		port(8080);
		
		staticFiles.externalLocation(new File(path).getCanonicalPath());
		webSocket("/chat", ChatWebSocketHandler.class);
		init();
		
		post("login", (req, res) -> {
			res.type("application/json");
			String userJSON = req.body();
			LoginUser u = g.fromJson(userJSON, LoginUser.class);
			if (!userDao.authenticateLogin(u.getUsername(), u.getPassword())) {
				res.status(401); // 401 - Unauthorized
				return "{\"status\": \"error\", \"info\": \"invalid username or password\"}";
			}
			else {
				Session ss = req.session(true);
				User user = ss.attribute("user");
				if (user == null) {
					user = userDao.getUser(u.getUsername());
					ss.attribute("user", user);
				}
				username = user.getUsername();
				return "{\"status\": \"success\"}";
			}
		});
		
		post("signup", (req, res) -> {
			res.type("application/json");
			String user = req.body();
			SignupUser signupUser = g.fromJson(user, SignupUser.class);
			
			UserValidator validator = new UserValidator(signupUser);
			if (userDao.userExists(signupUser.getSignupUsername())) {
				res.status(422); // 422 - Unprocessable entity error
				return "{\"status\":\"error\", \"info\":\"username taken\"}";
			} else if (!validator.isValid()) {
				res.status(422); // 422 - Unprocessable entity error
				return validator.getValidationJSON();
			}
			else {
				User u = new User(signupUser.getSignupUsername(), signupUser.getConfirmPassword(),
						signupUser.getEmail(), signupUser.getName(), signupUser.getSurname(), LocalDate.of(1970, 1, 1), signupUser.getGender(),
						 Role.USER, defaultProfilePicPath, false);
				userDao.addUser(u.getUsername(), u);
				userDao.updateFile();
				return validator.getValidationJSON();
			}
		});
		
		get("getuser", (req, res) -> {
			res.type("application/json");
			String errCode = req.queryParams("message");
			Session ss = req.session(true);
			User user = ss.attribute("user");
			if (errCode == null) {
				res.status(400);
				return "{\"status\": \"error\", \"info\":\"invalid request\"}";
			} else {
				if(user == null) {
					return "{\"status\": \"success\", \"info\": "+ g.toJson(userDao.getGuestUser()) +"}";
				}
				return "{\"status\": \"success\", \"info\": "+ g.toJson(user) +"}";
			}
		});
		
		get("getSelectedUser", (req, res) -> {
			res.type("application/json");
			String errCode = req.queryParams("message");
			User user = userDao.getUser(req.queryParams("username"));
			if (errCode == null) {
				res.status(400);
				return "{\"status\": \"error\", \"info\":\"invalid request\"}";
			} else {
				return "{\"status\": \"success\", \"info\": "+ g.toJson(user) +"}";
			}
		});
		
		get("logout", (req, res) -> {
			res.type("application/json");
			Session ss = req.session(true);
			User user = ss.attribute("user");
			if (user == null) {
				res.status(400);
				return "{\"status\": \"error\", \"info\":\"already logged out\"}";
			} else {
				ss.invalidate();
				return "{\"status\": \"success\"}";
			}
		});
		
		post("newPost", (req, res) -> {
			res.type("application/json");
			Session ss = req.session(true);
			User user = ss.attribute("user");
			if (user == null) {
				res.status(400);
				return "{\"status\": \"error\", \"info\":\"no logged user\"}";
			} else {
				MultipartConfigElement multipartConfigElement = new MultipartConfigElement("./data");
			    req.raw().setAttribute("org.eclipse.jetty.multipartConfig", multipartConfigElement);
			    Part file = req.raw().getPart("file"); 	            
			    String fullPath = null;
			    if(file.getSubmittedFileName() != null) {
			    	String generatedFileName = user.generateFilenameNext(file.getSubmittedFileName());
				    PostProcessor.writeImage(user.getPath(), generatedFileName, file);
				    fullPath = user.getPath() + generatedFileName;
			    }
			    String text = req.queryParams("text").equals("") ? null : req.queryParams("text");
			    String type = req.queryParams("type");
			    
			    Post post = new Post(userDao.generatePostId(), user.getUsername(),
			    					 fullPath,
			    					 text, PostType.valueOf(type.toUpperCase()));
			    user.addPost(post);
			    userDao.updateFile();
			    return "{\"status\": \"success\"}";
	        }
		});
		
		get("getProfilePhotoMap", (req, res) -> {
			res.type("application/json");
			String errCode = req.queryParams("message");
			if (errCode == null) {
				res.status(400);
				return "{\"status\": \"error\", \"info\":\"invalid request\"}";
			} else {
				return "{\"status\": \"success\", \"info\": "+ g.toJson(userDao.getProfilePhotoMap()) +"}";
			}
		});
		
		post("edit", (req, res) -> {
			res.type("application/json");
			Session ss = req.session(true);
			User user = ss.attribute("user");
			if (user == null) {
				res.status(400);
				return "{\"status\": \"error\", \"info\":\"no logged user\"}";
			} else {
				String editedUserStr = req.body();
				User editedUser = g.fromJson(editedUserStr, User.class);
				ss.attribute("user", editedUser);
				userDao.replaceUser(user.getUsername(), editedUser);
				userDao.updateFile();
				return "{\"status\": \"success\"}";
			}
		});

		
		get("getHomePosts", (req, res) -> {
			res.type("application/json");
			String code = req.queryParams("message");
			Session ss = req.session(true);
			User user = ss.attribute("user");
			if (code == null) {
				res.status(400);
				return "{\"status\": \"error\", \"info\":\"invalid request\"}";
			} else {
				return "{\"status\": \"success\", \"info\": "+ g.toJson(userDao.getPostsForHomepage(user)) +"}";
			}
		});
		
		put("deletePost", (req, res) -> {
			res.type("application/json");
			RequestBody reqBody = g.fromJson(req.body(), RequestBody.class);
			String id = reqBody.getId().split(":")[0];
			String author = reqBody.getId().split(":")[1];
			try{
				if (!reqBody.getMessage().equals("ok")) {
				res.status(400);
				return "{\"status\": \"error\", \"info\":\"invalid request\"}";
				} else {
					userDao.getUser(author).getPosts().forEach((post) -> {
						if(post.getId().equals(id)) {
							post.setDeleted();
							post.getComments().forEach((comm) -> {
								comm.setDeleted();
							});
						}
					});
					userDao.updateFile();
					return "{\"status\": \"success\"}";
				}
			} catch (Exception e) {
				res.status(400);
				return "{\"status\": \"error\", \"info\":\"invalid request\"}";
			}
		});
		
		post("newComment", (req, res) -> {
			res.type("application/json");
			RequestBody reqBody = g.fromJson(req.body(), RequestBody.class);
			Session ss = req.session(true);
			User user = ss.attribute("user");
			try{
				if (!reqBody.getMessage().equals("ok")) {
				res.status(400);
				return "{\"status\": \"error\", \"info\":\"invalid request\"}";
				} else {
					String postId = reqBody.getId().split(":")[0];
					String commentText = reqBody.getId().split(":")[1];
					Comment comment = new Comment(userDao.generateCommentId(),user.getUsername(), commentText);
					userDao.getPostById(postId).getComments().add(comment);
					userDao.updateFile();
					return "{\"status\": \"success\", \"info\": " + g.toJson(comment) + "}";
				}
			} catch (Exception e) {
				res.status(400);
				return "{\"status\": \"error\", \"info\":\"invalid request\"}";
			}
		});
		
		put("deleteComment", (req, res) -> {
			res.type("application/json");
			RequestBody reqBody = g.fromJson(req.body(), RequestBody.class);
			try{
				if (!reqBody.getMessage().equals("ok")) {
				res.status(400);
				return "{\"status\": \"error\", \"info\":\"invalid request\"}";
				} else {
					String commentId = reqBody.getId();
					userDao.getAllUsers().forEach((u) -> {
						u.getPosts().forEach((post) -> {
							post.getComments().forEach((c) -> {
								if(c.getId().equals(commentId))
									c.setDeleted();
							});
						});
					});
					userDao.updateFile();
					return "{\"status\": \"success\"}";
				}
			} catch (Exception e) {
				res.status(400);
				return "{\"status\": \"error\", \"info\":\"invalid request\"}";
			}			
		});
		
		put("editComment", (req, res) -> {
			res.type("application/json");
			RequestBody reqBody = g.fromJson(req.body(), RequestBody.class);
			try{
				if (!reqBody.getMessage().equals("ok")) {
				res.status(400);
				return "{\"status\": \"error\", \"info\":\"invalid request\"}";
				} else {
					String commentId = reqBody.getId().split(":")[0];
					String newText = reqBody.getId().split(":")[1];
					userDao.getAllUsers().forEach((u) -> {
						u.getPosts().forEach((post) -> {
							post.getComments().forEach((c) -> {
								if(c.getId().equals(commentId)) {
									c.setText(newText);
									c.setLastModified(LocalDateTime.now());
								}													
							});
						});
					});
					userDao.updateFile();
					return "{\"status\": \"success\"}";
				}
			} catch (Exception e) {
				res.status(400);
				return "{\"status\": \"error\", \"info\":\"invalid request\"}";
			}	
		});
		
		get("getSearchResults", (req, res) -> {
			res.type("application/json");
			String msg = req.queryParams("message");
			Type stringList = new TypeToken<ArrayList<String>>(){}.getType();
			ArrayList<String> nameSearch = g.fromJson(req.queryParams("searchParams"), stringList);
			Type dateList = new TypeToken<ArrayList<LocalDate>>(){}.getType();
			ArrayList<LocalDate> dateSearch = g.fromJson(req.queryParams("dateParams"), dateList);
			boolean isAdmin = g.fromJson(req.queryParams("isAdmin"), Boolean.class);
			if (msg.equals("ok")) {
				ArrayList<User> searchResult = userDao.getSearchResult(nameSearch, dateSearch, isAdmin);
				return "{\"status\": \"success\", \"info\": " + g.toJson(searchResult) + "}";
			} else {
				res.status(400);
				return "{\"status\": \"error\", \"info\":\"invalid request\"}";
			}
		});
		
		put("modifyFriendRequest", (req, res) -> {
			res.type("application/json");
			RequestBody reqBody = g.fromJson(req.body(), RequestBody.class);	
			Session ss = req.session(true);
			User user = ss.attribute("user");
			if (!reqBody.getMessage().equals("ok")) {
				res.status(400);
				return "{\"status\": \"error\", \"info\":\"invalid request\"}";
			} else {
				String id = reqBody.getId().split(":")[0];
				String newVal = reqBody.getId().split(":")[1];
				for(FriendRequest request : user.getFriendRequests()) {
					if(request.getId().equals(id)) {
						if(newVal.equals("true")) {
							request.setStatus(FriendRequestStatus.ACCEPTED);
							userDao.getUser(request.getRecipient()).addFriend(request.getSender());
							userDao.getUser(request.getSender()).addFriend(request.getRecipient());
						}
						else
							request.setStatus(FriendRequestStatus.DECLINED);
						break;
					}
				}
				userDao.updateFile();
				return "{\"status\": \"success\"}";
			}
		});
		
		put("removeFriend", (req, res) -> {
			res.type("application/json");
			RequestBody reqBody = g.fromJson(req.body(), RequestBody.class);
			Session ss = req.session(true);
			User user = ss.attribute("user");
			if (!reqBody.getMessage().equals("ok")) {
				res.status(400);
				return "{\"status\": \"error\", \"info\":\"invalid request\"}";
			} else {
				String id = reqBody.getId();
				user.getFriendList().remove(id);
				userDao.getUser(id).getFriendList().remove(user.getUsername());
				userDao.updateFile();
				return "{\"status\": \"success\"}";
			}			
		});
		
		post("addFriend", (req, res) -> {
			res.type("application/json");
			Session ss = req.session(true);
			User user = ss.attribute("user");
			RequestBody reqBody = g.fromJson(req.body(), RequestBody.class);
			if (!reqBody.getMessage().equals("ok")) {
				res.status(400);
				return "{\"status\": \"error\", \"info\":\"invalid request\"}";
			} else {
				String id = reqBody.getId();
				FriendRequest freq = new FriendRequest(userDao.generateFriendRequestId(), user.getUsername(), reqBody.getId());
				userDao.getUser(id).getFriendRequests().add(freq);
				userDao.updateFile();
				return "{\"status\": \"success\", \"info\": " + g.toJson(freq) + "}";
			}
		});
		
		get("getMessages", (req, res) -> {
			res.type("application/json");
			Session ss = req.session(true);
			User user = ss.attribute("user");
			String errCode = req.queryParams("message");
			if (!errCode.equals("ok")) {
				res.status(400);
				return "{\"status\": \"error\", \"info\":\"invalid request\"}";
			} else {
				String messagesWith = req.queryParams("target");
				ArrayList<DirectMessage> messages = messageDao.getMessagesForTwo(user.getUsername(), messagesWith);
				return "{\"status\": \"success\", \"info\": " + g.toJson(messages) + "}";
			}		
		});
		
		post("newMsg", (req, res) -> {
			res.type("application/json");
			Session ss = req.session(true);
			User user = ss.attribute("user");
			RequestBody reqBody = g.fromJson(req.body(), RequestBody.class);
			if (!reqBody.getMessage().split(":")[0].equals("ok")) {
				res.status(400);
				return "{\"status\": \"error\", \"info\":\"invalid request\"}";
			} else {
				String recipient = reqBody.getId();
				String text = reqBody.getMessage().split(":")[1];
				DirectMessage dm = new DirectMessage(messageDao.generateDMid(), user.getUsername(), recipient, text);
				messageDao.addMessage(dm.getId(), dm);
				messageDao.updateFile();
				return "{\"status\": \"success\", \"info\": " + g.toJson(dm) + "}";
			}
		});
		
		get("blockUser", (req, res) -> {
			res.type("application/json");
			String message = req.queryParams("message");
			String username = req.queryParams("username");
			boolean block = Boolean.parseBoolean(req.queryParams("block"));
			if(!message.equals("ok")) {
				res.status(400);
				return "{\"status\": \"error\", \"info\":\"invalid request\"}";
			} else {
				userDao.getUser(username).setBlocked(block);
				userDao.updateFile();
				return "{\"status\": \"success\"}";
			}
		});
		
		get("hasMsgWithAdmin", (req, res) -> {
			res.type("application/json");
			String message = req.queryParams("message");
			Session ss = req.session(true);
			User user = ss.attribute("user");
			if(!message.equals("ok")) {
				res.status(400);
				return "{\"status\": \"error\", \"info\":\"invalid request\"}";
			} else {
				ArrayList<String> toAdd = messageDao.adminsWithMsg(userDao, user.getUsername());
				return "{\"status\": \"success\", \"info\": " + g.toJson(toAdd) + "}";
			}
		});
	}
	
}
