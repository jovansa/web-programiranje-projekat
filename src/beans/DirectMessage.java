package beans;

import java.time.LocalDateTime;

/**
 * This is the DirectMessage class, which represents the message model in the 
 * application.
 * 
 * @author jovansa
 *
 */
public class DirectMessage {
	
	private String id;
	private String sender;
	private String recipient;
	private String text;
	private LocalDateTime timestamp;
	
	public DirectMessage(String id, String sender, String recipient, String text) {
		this.id = id;
		this.sender = sender;
		this.recipient = recipient;
		this.text = text;
		this.timestamp = LocalDateTime.now();
	}
	
	public String getId() {
		return this.id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getSender() {
		return this.sender;
	}
	
	public String getRecipient() {
		return this.recipient;
	}
	
	public String getText() {
		return this.text;
	}
	
	public LocalDateTime getTimestamp() {
		return this.timestamp;
	}
	
}
