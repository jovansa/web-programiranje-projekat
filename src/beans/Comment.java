package beans;

import java.time.LocalDateTime;

/**
 * This is the Comment class, which represents the comment model 
 * in the application.
 * 
 * @author jovansa
 *
 */
public class Comment {
	
	private String id;
	private String authorUsername;
	private String text;
	private LocalDateTime postDate;
	private LocalDateTime lastModified;
	private boolean isDeleted;
	
	public Comment(String id, String author, String text) {
		this.id = id;
		this.authorUsername = author;
		this.text = text;
		this.postDate = LocalDateTime.now();
		this.lastModified = null;
		this.isDeleted = false;
	}
	
	public String getId() {
		return this.id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getAuthorUsername() {
		return this.authorUsername;
	}
	
	public String getText() {
		return this.text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public LocalDateTime getPostDate() {
		return this.postDate;
	}
	
	public LocalDateTime getLastModified() {
		return this.lastModified;
	}
	
	public void setLastModified(LocalDateTime time) {
		this.lastModified = time;
	}
	
	public boolean isModified() {
		return this.lastModified == null ? true : false;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted() {
		this.isDeleted = true;
	}
	
}
