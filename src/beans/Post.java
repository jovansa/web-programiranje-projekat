package beans;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import enums.PostType;

/**
 * This is the Post class, which represents the post model in the application.
 * It covers both picture and text-only posts.
 * 
 * There are two types of posts: text-focused (called only 'posts') and 
 * image-focused (called 'image'). View requires to represent these two
 * differently.
 * 
 * @author jovansa
 *
 */
public class Post {
	
	private String id;
	private String authorUsername;
	private String image;
	private String text;
	private List<Comment> comments;
	private LocalDateTime timestamp;
	private PostType type;
	private boolean isDeleted;
	
	public Post(String id, String authorUsername, String image, String text, PostType type) {
		this.id = id;
		this.authorUsername = authorUsername;
		this.image = image;
		this.text = text;
		this.comments =  new ArrayList<Comment>();
		this.timestamp = LocalDateTime.now();
		this.type = type;
		isDeleted = false;
	}
	
	public String getId() {
		return this.id;
	}
	
	public String getAuthorUsername() {
		return this.authorUsername;
	}
	
	public String getImage() {
		return this.image;
	}
	
	public String getText() {
		return this.text;
	}
	
	public List<Comment> getComments(){
		return this.comments;
	}
	
	public LocalDateTime getTimestamp() {
		return this.timestamp;
	}
	
	public void setDeleted() {
		this.isDeleted = true;
	}
	
	public boolean isDeleted() {
		return this.isDeleted;
	}

	public PostType getType() {
		return this.type;
	}
}
