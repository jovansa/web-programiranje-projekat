package beans;



public class SignupUser {
	private String signupUsername;
	private String email;
	private String signupPassword;
	private String confirmPassword;
	private String name;
	private String surname;
	private String gender;
	
	public SignupUser(String username, String email, String firstPassword, String secondPassword,
					  String name, String surname, String gender) {
		this.signupUsername = username.toLowerCase();
		this.email = email;
		this.signupPassword = firstPassword;
		this.confirmPassword = secondPassword;
		this.name = name.substring(0,1).toUpperCase() + name.substring(1).toLowerCase();
		this.surname = surname.substring(0,1).toUpperCase() + surname.substring(1).toLowerCase();
		this.gender = gender;
	}

	public String getSignupUsername() {
		return signupUsername;
	}

	public void setSignupUsername(String signupUsername) {
		this.signupUsername = signupUsername;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSignupPassword() {
		return signupPassword;
	}

	public void setSignupPassword(String signupPassword) {
		this.signupPassword = signupPassword;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
	
}
