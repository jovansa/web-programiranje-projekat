package beans;

public class LoginUser {
	private String username;
	private String password;
	
	public LoginUser(String username, String password) {
		this.username = username.toLowerCase();
		this.password = password;
	}

	public String getUsername() {
		return username.toLowerCase();
	}

	public void setUsername(String name) {
		this.username = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String pass) {
		this.password = pass;
	}
	
}
