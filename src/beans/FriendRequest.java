package beans;

import java.time.LocalDateTime;

import enums.FriendRequestStatus;

/**
 * This is the FriendRequest class, which represents the friend request model in
 * the application.
 * 
 * @author jovansa
 *
 */
public class FriendRequest {
	
	private String id;
	private String sender;
	private String recipient;
	private FriendRequestStatus status;
	private LocalDateTime timestamp;
	
	public FriendRequest(String id, String sender, String recipient) {
		this.id = id;
		this.sender = sender;
		this.recipient = recipient;
		this.status = FriendRequestStatus.PENDING;
		this.timestamp = LocalDateTime.now();
	}
	
	public String getId() {
		return this.id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getSender() {
		return this.sender;
	}
	
	public String getRecipient() {
		return this.recipient;
	}
	
	public FriendRequestStatus getStatus() {
		return this.status;
	}
	
	public void setStatus(FriendRequestStatus status) {
		this.status = status;
	}
	
	public LocalDateTime getTimestamp() {
		return this.timestamp;
	}
	
}
