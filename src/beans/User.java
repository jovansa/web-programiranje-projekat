package beans;

import java.io.File;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import enums.Gender;
import enums.Role;

/**
 *	This is the User class, which represents the user model in the application.
 *	Since the username field is unique, that field is used as the ID.	
 *
 *	@author jovansa 
 *
 */
public class User {
	
	private static String imgPath = "./WebContent/users/";
	
	private String username;
	private String password;
	private String email;
	private String name;
	private String surname;
	private LocalDate dateOfBirth;
	private Gender gender;
	private Role role;
	private String profilePic;
	private List<Post> posts;
	private List<FriendRequest> friendRequests;
	private List<String> friendList;
	private boolean isPrivate;
	private boolean isBlocked;
	
	
	public User(String username, String password, String email, String name,
			String surname, LocalDate dateOfBirth, String gender, Role role, String profilePic,
			boolean isPrivate) {
		this.username = username.toLowerCase();
		this.password = password;
		this.email = email;
		this.name = name.substring(0,1).toUpperCase() + name.substring(1).toLowerCase();
		this.surname = surname.substring(0,1).toUpperCase() + surname.substring(1).toLowerCase();
		this.dateOfBirth = dateOfBirth;
		this.gender = Gender.valueOf(gender.toUpperCase());
		this.role = role;
		this.profilePic = profilePic;
		this.posts = new ArrayList<Post>();
		this.friendRequests = new ArrayList<FriendRequest>();
		this.friendList = new ArrayList<String>();
		this.isPrivate = isPrivate;
		this.isBlocked = false;
	
		new File(imgPath + this.username).mkdirs();
	}


	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getSurname() {
		return surname;
	}


	public void setSurname(String surname) {
		this.surname = surname;
	}


	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}


	public void setDateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}


	public Gender getGender() {
		return gender;
	}


	public void setGender(Gender gender) {
		this.gender = gender;
	}


	public Role getRole() {
		return role;
	}


	public void setRole(Role role) {
		this.role = role;
	}


	public String getProfilePic() {
		return profilePic;
	}


	public void setProfilePic(String profilePic) {
		this.profilePic = profilePic;
	}


	public List<Post> getPosts() {
		return posts;
	}


	public void addPost(Post post) {
		this.posts.add(post);
	}

	public List<FriendRequest> getFriendRequests() {
		return friendRequests;
	}


	public void addFriendRequest(FriendRequest request) {
		this.friendRequests.add(request);
	}


	public List<String> getFriendList() {
		return friendList;
	}

	public void addFriend(String u){
		this.friendList.add(u);
	}
	
	public boolean isPrivate() {
		return isPrivate;
	}

	public void setPrivate(boolean isPrivate) {
		this.isPrivate = isPrivate;
	}
	
	public void setBlocked(boolean isBlocked) {
		this.isBlocked = isBlocked;
	}
	
	public boolean isBlocked() {
		return this.isBlocked;
	}
	
	public String getPath() {
		return imgPath + this.username + "/";
	}
	
	public String generateFilenameNext(String submittedFilename) {
		String extension = "." + submittedFilename.split("\\.")[1];
		return this.username + String.valueOf(this.posts.size() + 1) + extension;
	}

	public ArrayList<String> getSearchTokens(boolean isAdmin){
		ArrayList<String> retVal = new ArrayList<String>();
		retVal.add(this.username);
		retVal.add(this.name);
		retVal.add(this.surname);
		if(isAdmin) retVal.add(this.email);
		return retVal;
	}
}
